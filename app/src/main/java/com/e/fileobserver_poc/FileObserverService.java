package com.e.fileobserver_poc;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Environment;

import android.os.IBinder;

import android.util.Log;

import androidx.annotation.Nullable;

import com.e.fileobserver_poc.RecursiveFileObserver;

import java.io.File;

public class FileObserverService extends Service {


    RecursiveFileObserver mFileObserver = null;


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Log.d("obService", "started");

        // onTaskRemoved(intent);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();


        Log.d("obService", "onCreate");
        String path = Environment.getExternalStorageDirectory().getAbsolutePath();

        Log.d("obService", path);
        mFileObserver = new RecursiveFileObserver(path, new RecursiveFileObserver.EventListener() {
            @Override
            public void onEvent(int event, File file) {

                Log.e("OnEvent", "...Event ..." + event);
                Log.e("OnEvent", "...file ..." + file);
            }
        });

        mFileObserver.startWatching();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        Log.e("TAG", "............OnDestroy");

        super.onDestroy();
    }


    @Override
    public void onTaskRemoved(Intent rootIntent) {

        Log.e("TAG", "............onTaskRemoved");
        try {
            Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
            restartServiceIntent.setPackage(getPackageName());

            startService(restartServiceIntent);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        super.onTaskRemoved(rootIntent);
    }

//
//
//    private boolean isMyServiceRunning(Class<?> serviceClass) {
//        ActivityManager manager = (ActivityManager) getSystemService(getApplication().ACTIVITY_SERVICE);
//        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
//            if (serviceClass.getName().equals(service.service.getClassName())) {
//                return true;
//            }
//        }
//        return false;
//    }

}
